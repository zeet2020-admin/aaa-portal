angular.module('aaa.services',[]).
factory('alerts', function($interval) {
    var alerts = undefined;
    if (!window.alertsInterval) {
        window.alertsInterval = $interval(function() {
            var alive = [];
            _.forEach(alerts, function(alert) {
                if (!moment().isAfter(moment(alert.timestamp).add(5, 'seconds'))) {
                    alive.push(alert);
                }
            });
            alerts = alive;
            store.set('alerts', alerts);
        }, 1000);
    }
    return {
        clear: function() {
            store.set('alerts', []);
        },
        get: function() {
            if (_.isUndefined(alerts)) {
                alerts = store.get('alerts');
            }
            if (_.isEmpty(alerts)) {
                alerts = [];
            }
            return alerts;
        },
        set: function(val) {
            alerts = val;
            store.set('alerts', alerts);
        },
        success: function(msg) {
            alerts.push({id: Math.random().toString(16), success: msg, timestamp: new Date().getTime()});
            store.set('alerts', alerts);
        },
        fail: function(msg) {
            alerts.push({id: Math.random().toString(16), danger: msg, timestamp: new Date().getTime()});
            store.set('alerts', alerts);
        }
    };
});
